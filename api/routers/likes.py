from typing import List
from fastapi import APIRouter, Depends
from queries.likes import LikeIn, LikeRepository, LikeOut


router = APIRouter()

@router.get('/api/{user_id}/likes/', response_model=List[LikeOut])
def get_by_user_id(
    user_id: int,
    repo: LikeRepository = Depends(),
):
    return repo.get_by_user_id(user_id)
