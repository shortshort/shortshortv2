from typing import List
from fastapi import APIRouter, Depends
from queries.dislikes import DislikeIn, DislikeRepository, DislikeOut


router = APIRouter()

@router.get('/api/{user_id}/dislikes/', response_model=List[DislikeOut])
def get_by_user_id(
    user_id: int,
    repo: DislikeRepository = Depends(),
):
    return repo.get_by_user_id(user_id)
