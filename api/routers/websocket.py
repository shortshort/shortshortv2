from fastapi import APIRouter, WebSocket, WebSocketDisconnect
from typing import List
import json
from queries.likes import LikeRepository
from queries.dislikes import DislikeRepository
from queries.submissions import SubmissionRepository, SubmissionIn
from queries.comments import CommentIn, CommentRepository

router = APIRouter()


class ConnectionManager:
    def __init__(self):
        self.active_connections: List[WebSocket] = []

    async def connect(self, websocket: WebSocket):
        await websocket.accept()
        self.active_connections.append(websocket)

    async def send_personal_message(self, message: str, websocket: WebSocket):
        await websocket.send_text(message)

    def disconnect(self, websocket: WebSocket):
        self.active_connections.remove(websocket)

    async def broadcast(self, message: str):
        for connection in self.active_connections:
            await connection.send_text(message)


manager = ConnectionManager()


def submission_convert(submission):
    sub_in = SubmissionIn(
        user_id=submission.user_id,
        content=submission.content,
        anon_flag=submission.anon_flag,
        created_at=submission.created_at,
        likes=submission.likes,
        dislikes=submission.dislikes,
        prompt_id=submission.prompt_id,
        title=submission.title
    )
    return sub_in


def sub_to_json(submission, isodate):
    return {
        "id": submission.id,
        "user_id": submission.user_id,
        "content": submission.content,
        "anon_flag": submission.anon_flag,
        "created_at": isodate,
        "likes": submission.likes,
        "dislikes": submission.dislikes,
        "prompt_id": submission.prompt_id,
        "title": submission.title
    }


@router.websocket("/ws/submissions")
async def websocket_endpoint_submissions(websocket: WebSocket):
    await manager.connect(websocket)
    sub_repository = SubmissionRepository()
    like_repository = LikeRepository()
    dislike_repo = DislikeRepository()

    try:
        while True:
            data = await websocket.receive_json()
            user_id = data["user_id"]
            submission_id = data["submission"]["id"]
            submission = sub_repository.get_submissions_by_id(submission_id)
            sub_in = submission_convert(submission)

            if not sub_in:
                await manager.send_personal_message(
                    "Invalid submission", websocket
                    )

            if data.get("message") == "like":
                like_repository.create_submission_like(user_id, submission_id)
                sub_in.likes += 1
                new_sub = sub_repository.update(submission_id, sub_in)
                created_at_iso = new_sub.created_at.isoformat()
                json_sub = sub_to_json(new_sub, created_at_iso)
                await manager.broadcast(json.dumps(json_sub))

            elif data.get("message") == "unlike":
                like_repository.delete_submission_like(user_id, submission_id)
                sub_in.likes -= 1
                new_sub = sub_repository.update(submission_id, sub_in)
                created_at_iso = new_sub.created_at.isoformat()
                json_sub = sub_to_json(new_sub, created_at_iso)
                await manager.broadcast(json.dumps(json_sub))

            elif data.get("message") == "dislike":
                dislike_repo.create_submission_dislike(user_id, submission_id)
                sub_in.dislikes += 1
                new_sub = sub_repository.update(submission_id, sub_in)
                created_at_iso = new_sub.created_at.isoformat()
                json_sub = sub_to_json(new_sub, created_at_iso)
                await manager.broadcast(json.dumps(json_sub))

            elif data.get("message") == "undislike":
                dislike_repo.delete_submission_dislike(user_id, submission_id)
                sub_in.dislikes -= 1
                new_sub = sub_repository.update(submission_id, sub_in)
                created_at_iso = new_sub.created_at.isoformat()
                json_sub = sub_to_json(new_sub, created_at_iso)
                await manager.broadcast(json.dumps(json_sub))

            else:
                await manager.send_personal_message(
                    "Invalid message", websocket
                    )

    except WebSocketDisconnect:
        manager.disconnect(websocket)

    except Exception as e:
        print("disconnected")
        print(f"Error: {e}")
        manager.disconnect(websocket)


# comment websocket endpoint

def comment_convert(comment):
    comment_in = CommentIn(
        user_id=comment.user_id,
        submission_id=comment.submission_id,
        content=comment.content,
        created_at=comment.created_at,
        likes=comment.likes,
        dislikes=comment.dislikes
    )
    return comment_in


def comment_to_json(comment, isodate):
    return {
        "id": comment.id,
        "user_id": comment.user_id,
        "submission_id": comment.submission_id,
        "content": comment.content,
        "created_at": isodate,
        "likes": comment.likes,
        "dislikes": comment.dislikes
    }


@router.websocket("/ws/comments")
async def websocket_endpoint_comments(websocket: WebSocket):
    await manager.connect(websocket)
    comment_repository = CommentRepository()
    like_repository = LikeRepository()
    dislike_repo = DislikeRepository()

    try:
        while True:
            data = await websocket.receive_json()
            user_id = data["user_id"]
            comment_id = data["comment"]["id"]
            comment = comment_repository.get_comments_by_id(comment_id)
            comment_in = comment_convert(comment)

            if data.get("message") == "like":
                like_repository.create_comment_like(user_id, comment_id)
                if comment_in:
                    comment_in.likes += 1
                    new_comment = comment_repository.update(comment_id, comment_in)
                    created_at_iso = new_comment.created_at.isoformat()
                    json_comment = comment_to_json(new_comment, created_at_iso)
                    await manager.broadcast(json.dumps(json_comment))

            elif data.get("message") == "unlike":
                like_repository.delete_comment_like(user_id, comment_id)
                if comment_in:
                    comment_in.likes -= 1
                    new_comment = comment_repository.update(comment_id, comment_in)
                    created_at_iso = new_comment.created_at.isoformat()
                    json_comment = comment_to_json(new_comment, created_at_iso)
                    await manager.broadcast(json.dumps(json_comment))

            elif data.get("message") == "dislike":
                dislike_repo.create_comment_dislike(user_id, comment_id)
                if comment_in:
                    comment_in.dislikes += 1
                    new_comment = comment_repository.update(comment_id, comment_in)
                    created_at_iso = new_comment.created_at.isoformat()
                    json_comment = comment_to_json(new_comment, created_at_iso)
                    await manager.broadcast(json.dumps(json_comment))

            elif data.get("message") == "undislike":
                dislike_repo.delete_comment_dislike(user_id, comment_id)
                if comment_in:
                    comment_in.dislikes -= 1
                    new_comment = comment_repository.update(comment_id, comment_in)
                    created_at_iso = new_comment.created_at.isoformat()
                    json_comment = comment_to_json(new_comment, created_at_iso)
                    await manager.broadcast(json.dumps(json_comment))

            else:
                await manager.send_personal_message(
                    "Invalid message", websocket
                    )

    except WebSocketDisconnect:
        manager.disconnect(websocket)

    except Exception as e:
        print("disconnected")
        print(f"Error: {e}")
        manager.disconnect(websocket)
