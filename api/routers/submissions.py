from typing import List
from fastapi import APIRouter, Depends
from queries.submissions import SubmissionIn, SubmissionRepository, SubmissionOut


router = APIRouter()


@router.post('/api/submissions/', response_model=SubmissionOut)
def create_submission(
    submission: SubmissionIn,
    repo: SubmissionRepository = Depends(),
):
    return repo.create(submission)


@router.get('/api/submissions/', response_model=List[SubmissionOut])
def get_submissions(
    repo: SubmissionRepository = Depends()
):
    return repo.get_all()


@router.get('/api/prompts/{prompt_id}/submissions/five', response_model=List[SubmissionOut])
def get_prompt_submissions(
    prompt_id: int,
    repo: SubmissionRepository = Depends(),
):
    return repo.get_top_five(prompt_id)


@router.get('/api/{user_id}/submissions/', response_model=List[SubmissionOut])
def get_all_submissions(
    user_id: int,
    repo: SubmissionRepository = Depends(),
):
    return repo.get_submissions_by_user_id(user_id)


@router.get('/api/prompts/{prompt_id}/submissions/', response_model=List[SubmissionOut])
def get_prompt_submissions(
    prompt_id: int,
    repo: SubmissionRepository = Depends(),
):
    return repo.get_submissions_by_prompt_id(prompt_id)



@router.get('/api/submissions/{submission_id}', response_model=SubmissionOut)
def get_one_submission(
    submission_id: int,
    repo: SubmissionRepository = Depends(),
):
    return repo.get_submissions_by_id(submission_id)


@router.put('/api/submissions/{submission_id}', response_model=SubmissionOut)
def update_submission(
    submission_id: int,
    submission: SubmissionIn,
    repo: SubmissionRepository = Depends(),
):
    return repo.update(submission_id, submission)


@router.delete('/api/submissions/{submission_id}', response_model=bool)
def delete_submission(
    submission_id: int,
    repo: SubmissionRepository = Depends(),
):
    return repo.delete(submission_id)
