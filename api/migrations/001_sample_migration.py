steps = [
    [
        """
        CREATE TABLE users (
            id SERIAL PRIMARY KEY NOT NULL,
            username VARCHAR(1000) UNIQUE NOT NULL,
            description VARCHAR(50) DEFAULT 'Fledgling Writer',
            location VARCHAR(50) DEFAULT 'Unknown',
            joined_at TIMESTAMPTZ DEFAULT NOW(),
            email VARCHAR(1000) NOT NULL,
            password VARCHAR(1000) NOT NULL,
            bio TEXT,
            avatar_picture VARCHAR(1000) DEFAULT 'https://merriam-webster.com/assets/mw/images/article/art-wap-article-main/egg-3442-e1f6463624338504cd021bf23aef8441@1x.jpg'
        );
        """,

        """
        DROP TABLE USERS;
        """
    ],
    [
        """
        CREATE TABLE prompt
        (
            id SERIAL PRIMARY KEY NOT NULL,
            content TEXT
        );
        """,
        """
        DROP TABLE prompt;
        """
    ],
    [
        """
        CREATE TABLE submission
        (
            id SERIAL PRIMARY KEY NOT NULL,
            user_id INT REFERENCES users("id") ON DELETE SET DEFAULT NULL,
            content TEXT,
            anon_flag BOOLEAN,
            created_at TIMESTAMPTZ,
            likes INT,
            dislikes INT,
            prompt_id INT REFERENCES prompt("id") ON DELETE SET DEFAULT NULL,
            title TEXT
        );
        """,

        """
        DROP TABLE submission;
        """
    ],
    [
        """
        CREATE TABLE comment
        (
            id SERIAL PRIMARY KEY NOT NULL,
            user_id INT REFERENCES users("id") ON DELETE SET DEFAULT NULL,
            submission_id INT REFERENCES submission("id") ON DELETE CASCADE,
            content TEXT,
            created_at TIMESTAMPTZ,
            likes INT,
            dislikes INT
        );
        """,
        """
        DROP TABLE comment;
        """
    ],
    [
        """
        Create Table likes
        (
            id SERIAL PRIMARY KEY NOT NULL,
            user_id INT REFERENCES users("id") ON DELETE SET DEFAULT NULL,
            submission_id INT REFERENCES submission("id") ON DELETE CASCADE,
            comment_id INT REFERENCES comment("id") ON DELETE CASCADE
        );
        """,

        """
        DROP TABLE likes;
        """
    ],
    [
        """
        CREATE TABLE dislikes
        (
            id SERIAL PRIMARY KEY NOT NULL,
            user_id INT REFERENCES users("id") ON DELETE SET DEFAULT NULL,
            submission_id INT REFERENCES submission("id") ON DELETE CASCADE,
            comment_id INT REFERENCES comment("id")  ON DELETE CASCADE
        );
        """,

        """
        DROP TABLE dislikes;
        """
    ]
]
