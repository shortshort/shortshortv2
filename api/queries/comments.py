from pydantic import BaseModel
from typing import List, Optional
from datetime import datetime
from queries.pool import pool


class CommentIn(BaseModel):
    user_id: int
    submission_id: int
    content: str
    created_at: datetime
    likes: int
    dislikes: int


class CommentOut(BaseModel):
    id: int
    user_id: Optional[int]
    submission_id: int
    content: str
    created_at: datetime
    likes: int
    dislikes: int


class CommentRepository:

    def create(self, comment: CommentIn) -> CommentOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO comment
                        (user_id,submission_id,content,created_at,likes,dislikes)
                    VALUES
                        (%s,%s,%s,%s,%s,%s)
                    RETURNING id;
                    """,
                    [
                        comment.user_id,
                        comment.submission_id,
                        comment.content,
                        comment.created_at,
                        comment.likes,
                        comment.dislikes
                    ]
                )
                id = result.fetchone()[0]
                old_data = comment.dict()
                return CommentOut(id=id, **old_data)

    def get_all(self) -> List[CommentOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                        id,
                        user_id,
                        submission_id,
                        content,
                        created_at,
                        likes,
                        dislikes
                        FROM comment
                        """
                    )
                    return [
                        self.record_to_comment_out(record)
                        for record in result
                    ]
        except Exception:
            return {"message": "Could not get submissions"}

    def get_comments_by_user(self, user_id: int) -> List[CommentOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, user_id, submission_id, content, created_at, likes, dislikes
                        FROM comment
                        WHERE user_id = %s;
                        """,
                        [user_id]
                    )
                    commentlist = []
                    rows = db.fetchall()
                    for row in rows:
                        comment = {
                            "id": row[0],
                            "user_id": row[1],
                            "submission_id": row[2],
                            "content": row[3],
                            "created_at": row[4],
                            "likes": row[5],
                            "dislikes": row[6]
                        }
                        commentlist.append(comment)
                    return commentlist
        except Exception:
            return {"message": "Could not get all comments"}

    def get_comments_by_submission(self, submission_id: int) -> List[CommentOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, user_id, submission_id, content, created_at, likes, dislikes
                        FROM comment
                        WHERE submission_id = %s;
                        """,
                        [submission_id]
                    )
                    commentlist = []
                    rows = db.fetchall()
                    for row in rows:
                        comment = {
                            "id": row[0],
                            "user_id": row[1],
                            "submission_id": row[2],
                            "content": row[3],
                            "created_at": row[4],
                            "likes": row[5],
                            "dislikes": row[6]
                        }
                        commentlist.append(comment)
                    return commentlist
        except Exception:
            return {"message": "Could not get all comments"}

    def get_comments_by_id(self, comment_id: int) -> CommentOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, user_id, submission_id, content, created_at, likes, dislikes
                        FROM comment
                        WHERE id = %s;
                        """,
                        [comment_id]
                    )
                    record = db.fetchone()
                    return self.record_to_comment_out(record)
        except Exception:
            return {"message": "Could not get comment"}

    def update(self, id: int, comment: CommentIn) -> CommentOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE comment
                        SET
                            user_id = %s,
                            submission_id = %s,
                            content = %s,
                            likes = %s,
                            dislikes = %s
                        WHERE id = %s
                        """,
                        [
                            comment.user_id,
                            comment.submission_id,
                            comment.content,
                            comment.likes,
                            comment.dislikes,
                            id
                        ]
                    )
                    old_data = comment.dict()
                    return CommentOut(id=id, **old_data)
        except Exception:
            return {"message": "Could not update comment"}

    def delete(self, comment_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM comment
                        WHERE id = %s
                        """,
                        [comment_id]
                    )
                    return True
        except Exception:
            return {"message": "Could not delete comment"}

    def record_to_comment_out(self, record):
        return CommentOut(
            id=record[0],
            user_id=record[1],
            submission_id=record[2],
            content=record[3],
            created_at=record[4],
            likes=record[5],
            dislikes=record[6],
        )
