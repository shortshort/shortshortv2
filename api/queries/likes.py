from pydantic import BaseModel
from queries.pool import pool
from typing import Optional


class LikeIn(BaseModel):
    user_id: int
    submission_id: Optional[int] = None
    comment_id: Optional[int] = None


class LikeOut(BaseModel):
    id: int
    user_id: int
    submission_id: Optional[int] = None
    comment_id: Optional[int] = None


class LikeRepository:
    def create_submission_like(self, user_id: int, submission_id: int) -> LikeOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    INSERT INTO likes
                        (user_id, submission_id)
                    VALUES
                        (%s, %s)
                    RETURNING id;
                    """,
                    [
                        user_id,
                        submission_id
                    ]
                )
                id = db.fetchone()[0]
                return LikeOut(
                    id=id,
                    user_id=user_id,
                    submission_id=submission_id)

    def create_comment_like(self, user_id: int, comment_id: int) -> LikeOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    INSERT INTO likes
                        (user_id, comment_id)
                    VALUES
                        (%s, %s)
                    RETURNING id;
                    """,
                    [
                        user_id,
                        comment_id
                    ]
                )
                id = db.fetchone()[0]
                return LikeOut(
                    id=id,
                    user_id=user_id,
                    comment_id=comment_id
                )

    def get_by_user_id(self, user_id: int) -> list[LikeOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT * FROM likes
                    WHERE user_id = %s;
                    """,
                    [
                        user_id
                    ]
                )
                return [LikeOut(
                    id=id,
                    user_id=user_id,
                    submission_id=submission_id,
                    comment_id=comment_id
                ) for id, user_id, submission_id, comment_id in db.fetchall()]

    def delete_submission_like(self, user_id: int, submission_id: int) -> None:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    DELETE FROM likes
                    WHERE user_id = %s AND submission_id = %s;
                    """,
                    [
                        user_id,
                        submission_id
                    ]
                )

    def delete_comment_like(self, user_id: int, comment_id: int) -> None:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    DELETE FROM likes
                    WHERE user_id = %s AND comment_id = %s;
                    """,
                    [
                        user_id,
                        comment_id
                    ]
                )
