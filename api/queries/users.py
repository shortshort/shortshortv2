from pydantic import BaseModel, validator
from typing import Optional, List
from datetime import datetime, timezone
from queries.pool import pool


class UserIn(BaseModel):
    username: str
    description: Optional[str]
    location: Optional[str]
    joined_at: Optional[str]
    email: str
    password: str
    bio: Optional[str]
    avatar_picture: Optional[str]


class UserInNoPassword(BaseModel):
    username: str
    description: Optional[str]
    location: Optional[str]
    joined_at: Optional[str]
    email: str
    bio: Optional[str]
    avatar_picture: Optional[str]


class UserOut(BaseModel):
    id: int
    username: str
    description: Optional[str]
    location: Optional[str]
    joined_at: Optional[str]
    email: str
    bio: Optional[str]
    avatar_picture: Optional[str]

    @validator("joined_at", pre=True)
    def format_joined_at(cls, v):
        if isinstance(v, datetime):
            return v.strftime("%Y-%m-%d %H:%M:%S")
        return v


class UserOutWithPassword(UserOut):
    hashed_password: str


class UserRepository:
    def create(
            self, user: UserIn
    ) -> UserOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO users (
                        username,
                        email,
                        password,
                        bio
                        )
                    VALUES (%s, %s, %s, %s)
                    RETURNING
                        id,
                        username,
                        email,
                        bio
                    """,
                    [
                        user.username,
                        user.email,
                        user.password,
                        user.bio
                    ],
                )
                id = result.fetchone()[0]
                new_user = UserOut(id=id, **user.dict())
                return new_user

    def get_all_users(self) -> List[UserOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id,
                    username,
                    description,
                    location,
                    joined_at,
                    email,
                    bio,
                    avatar_picture
                    FROM users
                    ORDER BY username
                    """
                )
                result = db.fetchall()
                users = []
                for record in result:
                    user = UserOut(
                        id=record[0],
                        username=record[1],
                        description=record[2],
                        location=record[3],
                        joined_at=record[4],
                        email=record[5],
                        bio=record[6],
                        avatar_picture=record[7],
                    )
                    users.append(user)
                return users

    def get(self, user_id: int) -> UserOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id,
                    username,
                    description,
                    location,
                    joined_at,
                    email,
                    bio,
                    avatar_picture
                    FROM users
                    WHERE id = %s
                    """,
                    [user_id],
                )
                record = db.fetchone()
                if record is None:
                    return None
                user = UserOut(
                    id=record[0],
                    username=record[1],
                    description=record[2],
                    location=record[3],
                    joined_at=record[4],
                    email=record[5],
                    bio=record[6],
                    avatar_picture=record[7],
                )
                return user

    def get_username(self, username: str) -> UserOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id,
                    username,
                    description,
                    location,
                    joined_at,
                    email,
                    password,
                    bio,
                    avatar_picture
                    FROM users
                    WHERE username = %s
                    """,
                    [username],
                )
                record = db.fetchone()
                if record is None:
                    return None
                user = UserOutWithPassword(
                    id=record[0],
                    username=record[1],
                    description=record[2],
                    location=record[3],
                    joined_at=record[4],
                    email=record[5],
                    hashed_password=record[6],
                    bio=record[7],
                    avatar_picture=record[8],
                )
                return user

    def delete(self, user_id: int) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    DELETE FROM users
                    WHERE id = %s
                    """,
                    [user_id],
                )
                return bool(result.rowcount)

    def update(self, user_id: int, user: UserInNoPassword) -> UserOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    UPDATE users
                    SET
                        username = %s,
                        location = %s,
                        description = %s,
                        email = %s,
                        bio = %s,
                        avatar_picture = %s
                    WHERE id = %s
                    RETURNING
                        id,
                        username,
                        location,
                        description,
                        email,
                        bio,
                        avatar_picture
                    """,
                    [
                        user.username,
                        user.location,
                        user.description,
                        user.email,
                        user.bio,
                        user.avatar_picture,
                        user_id,
                    ],
                )
                record = result.fetchone()
                if record is None:
                    return None
                user = UserOut(
                    id=record[0],
                    username=record[1],
                    description=record[2],
                    location=record[3],
                    email=record[4],
                    bio=record[5],
                    avatar_picture=record[6],
                )
                return user
