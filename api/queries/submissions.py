from pydantic import BaseModel
from typing import List, Optional
from datetime import datetime
from queries.pool import pool
from fastapi import HTTPException


class SubmissionIn(BaseModel):
    user_id: Optional[int]
    content: str
    anon_flag: bool
    created_at: datetime
    likes: int
    dislikes: int
    prompt_id: int
    title: str


class SubmissionOut(BaseModel):
    id: int
    user_id: Optional[int]
    content: str
    anon_flag: bool
    created_at: datetime
    likes: int
    dislikes: int
    prompt_id: Optional[int]
    title: str


class SubmissionRepository:
    def create(self, submission: SubmissionIn) -> SubmissionOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO submission
                        (user_id,content,anon_flag,created_at,likes,dislikes,prompt_id,title)
                    VALUES
                        (%s,%s,%s,%s,%s,%s,%s,%s)
                    RETURNING id;
                    """,
                    [
                        submission.user_id,
                        submission.content,
                        submission.anon_flag,
                        submission.created_at,
                        submission.likes,
                        submission.dislikes,
                        submission.prompt_id,
                        submission.title,
                    ]
                )
                id = result.fetchone()[0]
                old_data = submission.dict()
                return SubmissionOut(id=id, **old_data)

    def get_all(self) -> List[SubmissionOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                        id,
                        user_id,
                        content,
                        anon_flag,
                        created_at,
                        likes,
                        dislikes,
                        prompt_id,
                        title
                        FROM submission
                        """
                    )
                    return [
                        self.record_to_submission_out(record)
                        for record in result
                    ]
        except Exception:
            return {"message": "Could not get submissions"}

    def get_top_five(self, prompt_id: int) -> List[SubmissionOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                        id,
                        user_id,
                        content,
                        anon_flag,
                        created_at,
                        likes,
                        dislikes,
                        prompt_id,
                        title
                        FROM submission
                        WHERE prompt_id = %s
                        ORDER BY (likes-dislikes) DESC
                        LIMIT 5
                        """,
                        [prompt_id]
                    )
                    return [
                        self.record_to_submission_out(record)
                        for record in result
                    ]
        except Exception as e:
            print(f"Error fetching submission by id: {e}")
            raise HTTPException(status_code=500, detail="Internal Server Error")

    def get_submissions_by_id(self, submission_id: int) -> SubmissionOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, user_id, content, anon_flag, created_at, likes, dislikes, prompt_id, title
                        FROM submission
                        WHERE id = %s
                        """,
                        [submission_id]
                    )
                    record = db.fetchone()
                    if record is None:
                        raise HTTPException(status_code=404, detail="No submission found with that id")

                    return SubmissionOut(
                        id=record[0],
                        user_id=record[1],
                        content=record[2],
                        anon_flag=record[3],
                        created_at=record[4],
                        likes=record[5],
                        dislikes=record[6],
                        prompt_id=record[7],
                        title=record[8]
                    )
        except Exception as e:
            print(f"Error fetching submission by id: {e}")
            raise HTTPException(status_code=500, detail="Internal Server Error")

    def get_submissions_by_user_id(self, user_id: int) -> List[SubmissionOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, user_id, content, anon_flag, created_at, likes, dislikes, prompt_id, title
                        FROM submission
                        WHERE user_id = %s
                        """,
                        [user_id]
                    )
                    submissionlist = []
                    rows = db.fetchall()
                    for row in rows:
                        submission = {
                            "id": row[0],
                            "user_id": row[1],
                            "content": row[2],
                            "anon_flag": row[3],
                            "created_at": row[4],
                            "likes": row[5],
                            "dislikes": row[6],
                            "prompt_id": row[7],
                            "title": row[8]
                        }
                        submissionlist.append(submission)
                    return submissionlist
        except Exception:
            return {"message": "Could not get all submissions"}

    def get_submissions_by_prompt_id(self, prompt_id: int) -> List[SubmissionOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, user_id, content, anon_flag, created_at, likes, dislikes, prompt_id, title
                        FROM submission
                        WHERE prompt_id = %s AND (user_id IS NULL OR user_id <> 0)
                        """,
                        [prompt_id]
                    )
                    submissionlist = []
                    rows = db.fetchall()
                    for row in rows:
                        submission = {
                            "id": row[0],
                            "user_id": row[1],
                            "content": row[2],
                            "anon_flag": row[3],
                            "created_at": row[4],
                            "likes": row[5],
                            "dislikes": row[6],
                            "prompt_id": row[7],
                            "title": row[8]
                        }
                        submissionlist.append(submission)
                    return submissionlist
        except Exception:
            return {"message": "Could not get all submissions"}

    def update(self, id: int, submission: SubmissionIn) -> SubmissionOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE submission
                        SET
                            user_id = %s,
                            content = %s,
                            anon_flag = %s,
                            likes = %s,
                            dislikes = %s,
                            title = %s
                        WHERE id = %s
                        """,
                        [
                            submission.user_id,
                            submission.content,
                            submission.anon_flag,
                            submission.likes,
                            submission.dislikes,
                            submission.title,
                            id,
                        ]
                    )
                    old_data = submission.dict()
                    return SubmissionOut(id=id, **old_data)
        except Exception as e:
            return {"message": str(e)}

    def delete(self, submission_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM submission
                        WHERE id = %s
                        """,
                        [submission_id]
                    )
                    return True
        except Exception:
            return {"message": "Could not delete submission"}

    def record_to_submission_out(self, record):
        return SubmissionOut(
            id=record[0],
            user_id=record[1],
            content=record[2],
            anon_flag=record[3],
            created_at=record[4],
            likes=record[5],
            dislikes=record[6],
            prompt_id=record[7],
            title=record[8]
        )
