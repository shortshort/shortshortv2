from fastapi import FastAPI
from routers import users, submissions, comments, prompts, websocket, likes, dislikes
from fastapi.middleware.cors import CORSMiddleware
from authenticator import authenticator
import os


app = FastAPI()
app.include_router(authenticator.router)
app.include_router(users.router)
app.include_router(submissions.router)
app.include_router(comments.router)
app.include_router(prompts.router)
app.include_router(likes.router)
app.include_router(dislikes.router)
app.include_router(websocket.router)

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        os.environ.get("CORS_HOST", "http://localhost:3000")
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def root():
    return {"message": "This is the root path."}
