import { useParams, Link } from "react-router-dom";
import { useEffect, useState, Fragment } from "react";
import { Tab, Listbox, Transition } from "@headlessui/react";
import { Pagination } from "flowbite-react";
import { CheckIcon, ChevronUpDownIcon } from "@heroicons/react/20/solid";
import dayjs from "dayjs";
import advancedFormat from "dayjs/plugin/advancedFormat";
import {
  fetchUser,
  fetchUserSubmissions,
  fetchUserComments,
} from "./actions/queries";

export default function UserDetail({ userData }) {
  const [loading, setLoading] = useState(true);
  const [user, setUser] = useState({});
  const [editedUser, setEditedUser] = useState(user ? { ...user } : {});
  const sort = [
    { id: 1, name: "Latest" },
    { id: 2, name: "Oldest" },
    { id: 3, name: "Most Popular" },
    { id: 4, name: "Least Popular" },
  ];
  const [selectedSort, setSelectedSort] = useState(sort[0]);
  const [submissions, setSubmissions] = useState([]);
  const [currentSubmissions, setCurrentSubmissions] = useState([]);
  const [comments, setComments] = useState([]);
  const [currentComments, setCurrentComments] = useState([]);
  const [currentSubmissionPage, setCurrentSubmissionPage] = useState(1);
  const [currentCommentPage, setCurrentCommentPage] = useState(1);
  const [submissionsPerPage] = useState(5);
  const [commentsPerPage] = useState(5);
  const { id } = useParams();
  const [isEditing, setIsEditing] = useState(false);

  dayjs.extend(advancedFormat);

  // Sorting

  function sortPosts(posts, sort) {
    switch (sort.id) {
      case 1:
        return posts.sort(
          (a, b) => new Date(b.created_at) - new Date(a.created_at)
        );
      case 2:
        return posts.sort(
          (a, b) => new Date(a.created_at) - new Date(b.created_at)
        );
      case 3:
        return posts.sort((a, b) => b.likes - a.likes);
      case 4:
        return posts.sort((a, b) => a.likes - b.likes);
      default:
        return posts;
    }
  }
  const handleEditToggle = () => {
    setIsEditing(!isEditing);
    if (!isEditing) {
      setEditedUser({ ...user });
    }
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setEditedUser((prevUser) => ({
      ...prevUser,
      [name]: value,
    }));
  };

  const handleSaveChanges = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_HOST}/api/users/${id}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(editedUser),
        }
      );
      if (response.ok) {
        setUser(editedUser);
        setIsEditing(false);
      }
    } catch (error) {
      console.error("An error occurred while saving the changes: ", error);
    }
  };

  const onSubmissionPageChange = (page) => setCurrentSubmissionPage(page);
  const indexOfLastSubmission = currentSubmissionPage * submissionsPerPage;
  const indexOfFirstSubmission = indexOfLastSubmission - submissionsPerPage;
  const submissionPages = Math.ceil(submissions?.length / submissionsPerPage);
  const onCommentPageChange = (page) => setCurrentCommentPage(page);
  const indexOfLastComment = currentCommentPage * commentsPerPage;
  const indexOfFirstComment = indexOfLastComment - commentsPerPage;
  const commentPages = Math.ceil(comments?.length / commentsPerPage);

  function classNames(...classes) {
    return classes.filter(Boolean).join(" ");
  }

  useEffect(() => {
    const fetchData = async () => {
      const user = await fetchUser(id);
      setUser(user);
      setEditedUser({ ...user });
      setLoading(false);

      const comments = await fetchUserComments(id);
      setComments(sortPosts(comments, selectedSort));
      setCurrentComments(
        comments.slice(indexOfFirstComment, indexOfLastComment)
      );

      const submissions = await fetchUserSubmissions(id);

      let filteredSubmissions;

      if (userData && String(user.id) === String(userData.id)) {
        filteredSubmissions = submissions;
      } else {
        filteredSubmissions = submissions.filter((sub) => !sub.anon_flag);
      }

      setSubmissions(sortPosts(filteredSubmissions, selectedSort));
      setCurrentSubmissions(
        filteredSubmissions.slice(indexOfFirstSubmission, indexOfLastSubmission)
      );
    };
    fetchData();
  }, [id]);

  useEffect(() => {
    setSubmissions(sortPosts(submissions, selectedSort));
    setCurrentSubmissions(
      submissions?.slice(indexOfFirstSubmission, indexOfLastSubmission)
    );
  }, [selectedSort, currentSubmissionPage]);

  useEffect(() => {
    setComments(sortPosts(comments, selectedSort));
    setCurrentComments(
      comments?.slice(indexOfFirstComment, indexOfLastComment)
    );
  }, [selectedSort, currentCommentPage]);

  if (loading) {
    return <p>Loading...</p>;
  } else {
    return (
      <>
        <div className="container px-4 mx-auto mt-12">
          <div className="grid grid-cols-2">
            {/* User Card */}
            <div className="w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
              <div className="flex justify-end px-4 pt-4">
                {!isEditing && (
                  <button
                    onClick={handleEditToggle}
                    className="px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 dark:text-gray-200 dark:hover:bg-gray-600 dark:hover:text-white"
                  >
                    Edit
                  </button>
                )}
              </div>
              <div className="flex flex-col items-center pb-10">
                <img
                  className="w-24 h-24 mb-3 rounded-full shadow-lg"
                  src={user?.avatar_picture}
                  alt="avatar"
                />
                {isEditing ? (
                  <>
                    <label
                      htmlFor="avatar_picture"
                      className="flex items-center justify-center mb-1 text-sm"
                    >
                      AvatarURL
                    </label>
                    <input
                      type="text"
                      name="avatar_picture"
                      id="avatar_picture"
                      value={editedUser?.avatar_picture || ""}
                      onChange={handleInputChange}
                      className="mb-1 text-sm text-gray-500 dark:text-gray-400"
                    />
                    <label htmlFor="location" className="mb-1 text-sm">
                      Location (Optional)
                    </label>
                    <input
                      type="text"
                      id="location"
                      name="location"
                      value={editedUser?.location || ""}
                      onChange={handleInputChange}
                      className="mb-1 text-sm text-gray-500 dark:text-gray-400"
                    />
                    <label htmlFor="description" className="mb-1 text-sm">
                      Description
                    </label>
                    <input
                      type="text"
                      id="description"
                      name="description"
                      value={editedUser?.description || ""}
                      onChange={handleInputChange}
                      className="mb-1 text-sm text-gray-500 dark:text-gray-400"
                    />
                  </>
                ) : (
                  <>
                    <h5 className="mb-1 text-xl font-medium text-gray-900 dark:text-white">
                      {user?.username}
                    </h5>
                    <span className="text-sm text-gray-500 dark:text-gray-400">
                      {user?.location}
                    </span>
                    <span className="text-sm text-gray-500 dark:text-gray-400">
                      {user?.description}
                    </span>
                    <span className="text-sm text-gray-500 dark:text-gray-400">
                      Joined on {dayjs(user?.joined_at).format("MMMM Do, YYYY")}
                    </span>
                  </>
                )}

                <div className="mt-6 mb-3 flex gap-14 md:!gap-14">
                  <div className="flex flex-col items-center justify-center">
                    <p className="text-2xl font-bold text-navy-700 dark:text-white">
                      {submissions?.length}
                    </p>
                    <p className="text-sm font-normal text-gray-600">
                      Submissions
                    </p>
                  </div>
                  <div className="flex flex-col items-center justify-center">
                    <p className="text-2xl font-bold text-navy-700 dark:text-white">
                      {comments?.length}
                    </p>
                    <p className="text-sm font-normal text-gray-600">
                      Comments
                    </p>
                  </div>
                </div>
              </div>
              {/* User Bio Card */}
              <div className="block max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
                <label htmlFor="bio" className="mb-1 text-sm">
                  Biography
                </label>
                {isEditing ? (
                  <input
                    type="text"
                    id="bio"
                    name="bio"
                    value={editedUser?.bio || ""}
                    onChange={handleInputChange}
                    className="font-normal text-gray-700 dark:text-gray-400"
                  />
                ) : (
                  <p className="font-normal text-gray-700 dark:text-gray-400">
                    {user?.bio}
                  </p>
                )}
              </div>
              {isEditing && (
                <>
                  <button
                    onClick={handleSaveChanges}
                    className="px-4 py-2 mt-4 text-sm text-white bg-blue-500 rounded hover:bg-blue-600"
                  >
                    Save Changes
                  </button>
                  <button
                    onClick={handleEditToggle}
                    className="px-4 py-2 mt-4 text-sm text-white bg-red-500 rounded hover:bg-red-600"
                  >
                    Cancel
                  </button>
                </>
              )}
            </div>

            {/* Tab */}
            <div className="col-span-1">
              <div className="w-full max-w-md px-2 py-16 sm:px-0">
                <Tab.Group>
                  <Tab.List className="flex p-1 space-x-1 rounded-xl bg-blue-900/20">
                    <Tab
                      key="submissions"
                      className={({ selected }) =>
                        classNames(
                          "w-full rounded-lg py-2.5 text-sm font-medium leading-5",
                          "ring-white/60 ring-offset-2 ring-offset-blue-400 focus:outline-none focus:ring-2",
                          selected
                            ? "bg-white text-blue-700 shadow"
                            : "text-blue-100 hover:bg-white/[0.12] hover:text-white"
                        )
                      }
                    >
                      Submission
                    </Tab>
                    <Tab
                      key="comments"
                      className={({ selected }) =>
                        classNames(
                          "w-full rounded-lg py-2.5 text-sm font-medium leading-5",
                          "ring-white/60 ring-offset-2 ring-offset-blue-400 focus:outline-none focus:ring-2",
                          selected
                            ? "bg-white text-blue-700 shadow"
                            : "text-blue-100 hover:bg-white/[0.12] hover:text-white"
                        )
                      }
                    >
                      Comments
                    </Tab>
                  </Tab.List>
                  <div className="w-72">
                    <Listbox value={selectedSort} onChange={setSelectedSort}>
                      <div className="relative mt-1">
                        <Listbox.Button className="relative w-full py-2 pl-3 pr-10 text-left bg-white rounded-lg shadow-md cursor-default focus:outline-none focus-visible:border-indigo-500 focus-visible:ring-2 focus-visible:ring-white/75 focus-visible:ring-offset-2 focus-visible:ring-offset-orange-300 sm:text-sm">
                          <span className="block truncate">
                            {selectedSort.name}
                          </span>
                          <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                            <ChevronUpDownIcon
                              className="w-5 h-5 text-gray-400"
                              aria-hidden="true"
                            />
                          </span>
                        </Listbox.Button>
                        <Transition
                          as={Fragment}
                          leave="transition ease-in duration-100"
                          leaveFrom="opacity-100"
                          leaveTo="opacity-0"
                        >
                          <Listbox.Options className="absolute z-10 w-full py-1 mt-1 overflow-auto text-base bg-white rounded-md shadow-lg max-h-60 ring-1 ring-black/5 focus:outline-none sm:text-sm">
                            {sort.map((sort) => (
                              <Listbox.Option
                                key={sort.id}
                                className={({ active }) =>
                                  `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                    active
                                      ? "bg-amber-100 text-amber-900"
                                      : "text-gray-900"
                                  }`
                                }
                                value={sort}
                              >
                                {({ selectedSort }) => (
                                  <>
                                    <span
                                      className={`block truncate ${
                                        selectedSort
                                          ? "font-medium"
                                          : "font-normal"
                                      }`}
                                    >
                                      {sort.name}
                                    </span>
                                    {selectedSort ? (
                                      <span className="absolute inset-y-0 left-0 flex items-center pl-3 text-amber-600">
                                        <CheckIcon
                                          className="w-5 h-5"
                                          aria-hidden="true"
                                        />
                                      </span>
                                    ) : null}
                                  </>
                                )}
                              </Listbox.Option>
                            ))}
                          </Listbox.Options>
                        </Transition>
                      </div>
                    </Listbox>
                  </div>
                  <Tab.Panels className="mt-2">
                    {submissions && (
                      <Tab.Panel
                        key="submissions"
                        className={classNames(
                          "rounded-xl bg-white p-3",
                          "ring-white/60 ring-offset-2 ring-offset-blue-400 focus:outline-none focus:ring-2"
                        )}
                      >
                        <ul>
                          {currentSubmissions.map((submission) => (
                            <li
                              key={submission.id}
                              className="relative p-3 rounded-md hover:bg-gray-100"
                            >
                              <h3 className="text-sm font-medium leading-5">
                                {submission.title}
                              </h3>

                              <ul className="flex mt-1 space-x-1 text-xs font-normal leading-4 text-gray-500">
                                <li>
                                  {dayjs(submission.created_at).format(
                                    "MMMM Do, YYYY"
                                  )}
                                </li>
                                <li>&middot;</li>
                                <li>{submission.likes} likes</li>
                                <li>&middot;</li>
                                <li>{submission.dislikes} dislikes</li>
                              </ul>

                              <Link
                                to={`/submissions/${submission.id}`}
                                className={classNames(
                                  "absolute inset-0 rounded-md",
                                  "ring-blue-400 focus:z-10 focus:outline-none focus:ring-2"
                                )}
                              />
                            </li>
                          ))}
                        </ul>
                        <div className="flex overflow-x-auto sm:justify-center">
                          <Pagination
                            currentPage={currentSubmissionPage}
                            totalPages={submissionPages}
                            onPageChange={onSubmissionPageChange}
                          />
                        </div>
                      </Tab.Panel>
                    )}
                    {comments && (
                      <Tab.Panel
                        key="comments"
                        className={classNames(
                          "rounded-xl bg-white p-3",
                          "ring-white/60 ring-offset-2 ring-offset-blue-400 focus:outline-none focus:ring-2"
                        )}
                      >
                        <ul>
                          {currentComments.map((comment) => (
                            <li
                              key={comment.id}
                              className="relative p-3 rounded-md hover:bg-gray-100"
                            >
                              <h3 className="text-sm font-medium leading-5">
                                {comment.content}
                              </h3>

                              <ul className="flex mt-1 space-x-1 text-xs font-normal leading-4 text-gray-500">
                                <li>
                                  {dayjs(comment.created_at).format(
                                    "MMMM Do, YYYY"
                                  )}
                                </li>
                                <li>&middot;</li>
                                <li>{comment.likes} likes</li>
                                <li>&middot;</li>
                                <li>{comment.dislikes} dislikes</li>
                              </ul>

                              <Link
                                to={`/submissions/${comment.submission_id}`}
                                className={classNames(
                                  "absolute inset-0 rounded-md",
                                  "ring-blue-400 focus:z-10 focus:outline-none focus:ring-2"
                                )}
                              />
                            </li>
                          ))}
                        </ul>
                        <div className="flex overflow-x-auto sm:justify-center">
                          <Pagination
                            currentPage={currentCommentPage}
                            totalPages={commentPages}
                            onPageChange={onCommentPageChange}
                          />
                        </div>
                      </Tab.Panel>
                    )}
                  </Tab.Panels>
                </Tab.Group>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
