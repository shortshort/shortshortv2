import React, { useEffect, useState, Fragment } from "react";
import SubmissionThread from "./SubmissionThread";
import {
  Pagination,
  Checkbox,
  Button,
  Label,
  TextInput,
  Textarea,
} from "flowbite-react";
import { Listbox, Transition } from "@headlessui/react";
import { CheckIcon, ChevronUpDownIcon } from "@heroicons/react/20/solid";
import dayjs from "dayjs";

import {
  fetchSubmissions,
  fetchOnePrompt,
  fetchLikes,
  fetchDislikes,
} from "./actions/queries";

const Main = ({ token, userData, currentDateTime, socket }) => {
  // useState variables
  const [prompt, setPrompt] = useState({ id: 0, content: "" });
  const [submission, setSubmission] = useState({
    user_id: 0,
    content: "",
    anon_flag: false,
    created_at: currentDateTime,
    likes: 0,
    dislikes: 0,
    prompt_id: prompt.id,
    title: "",
  });
  const sort = [
    { id: 1, name: "Latest" },
    { id: 2, name: "Oldest" },
    { id: 3, name: "Most Popular" },
    { id: 4, name: "Least Popular" },
  ];
  const [selectedSort, setSelectedSort] = useState(sort[0]);
  const [anon, setAnon] = useState(false);
  const [submissions, setSubmissions] = useState([]);
  const [likeStates, setLikeStates] = useState({});
  const [dislikeStates, setDislikeStates] = useState({});
  const [currentSubmissionPage, setCurrentSubmissionPage] = useState(1);
  const [currentSubmissions, setCurrentSubmissions] = useState([]);
  const [submissionsPerPage] = useState(5);

  // Sorting

  function sortSubmissions(submissions, sort) {
    switch (sort.id) {
      case 1:
        return submissions.sort(
          (a, b) => new Date(b.created_at) - new Date(a.created_at)
        );
      case 2:
        return submissions.sort(
          (a, b) => new Date(a.created_at) - new Date(b.created_at)
        );
      case 3:
        return submissions.sort((a, b) => b.likes - a.likes);
      case 4:
        return submissions.sort((a, b) => a.likes - b.likes);
      default:
        return submissions;
    }
  }

  // Pagination
  const onSubmissionPageChange = (page) => setCurrentSubmissionPage(page);
  const submissionPages = Math.ceil(submissions?.length / submissionsPerPage);
  const indexOfLastSubmission = currentSubmissionPage * submissionsPerPage;
  const indexOfFirstSubmission = indexOfLastSubmission - submissionsPerPage;

  useEffect(() => {
    setSubmissions(sortSubmissions(submissions, selectedSort));
    setCurrentSubmissions(
      submissions?.slice(indexOfFirstSubmission, indexOfLastSubmission)
    );
  }, [selectedSort, currentSubmissionPage]);

  // Change Prompt every 24 hours
  let count = 1;

  function Count() {
    count += 1;
  }

  setInterval(Count, 1000 * 60 * 60 * 24);
  const date = dayjs();

  // Post Submission
  const postSubmission = async (e) => {
    e.preventDefault();
    const submissionData = {
      user_id: userData ? userData.id : null,
      content: submission.content,
      anon_flag: anon,
      created_at: date.$d,
      likes: 0,
      dislikes: 0,
      prompt_id: prompt.id,
      title: submission.title,
    };

    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_HOST}/api/submissions/`,
        {
          method: "POST",
          body: JSON.stringify(submissionData),
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      if (response.ok) {
        window.location.reload();
      } else {
        console.error("Failed to post submission:", response.statusText);
      }
    } catch (error) {
      console.error("Error:");
    }
  };

  // fetch submissions and prompt
  useEffect(() => {
    const fetchData = async () => {
      const submissions = await fetchSubmissions();
      setSubmissions(sortSubmissions(submissions, selectedSort));
      setCurrentSubmissions(
        submissions.slice(indexOfFirstSubmission, indexOfLastSubmission)
      );

      const prompt = await fetchOnePrompt(count);
      setPrompt(prompt);
    };
    fetchData();
    // eslint-disable-next-line
  }, []);

  //fetches likes and dislikes for the specific user
  // only fetches if user is authenticated
  useEffect(() => {
    const fetchData = async () => {
      if (userData) {
        const likes = await fetchLikes(userData);
        const dislikes = await fetchDislikes(userData);
        setLikeStates(likes);
        setDislikeStates(dislikes);
      }
    };
    fetchData();
    // eslint-disable-next-line
  }, [userData]);

  return (
    <div className="container mx-auto mt-12">
      <h2 className="text-4xl font-extrabold">{prompt?.content}</h2>
      <div>
        <div>
          <form onSubmit={(e) => postSubmission(e)}>
            <div className="py-4">
              <TextInput
                required
                value={submission.title}
                onChange={(e) =>
                  setSubmission({ ...submission, title: e.target.value })
                }
                placeholder="Title"
                className="w-1/2"
              />
            </div>
            <div className="pb-4">
              <Textarea
                required
                maxLength={500}
                value={submission.content}
                onChange={(e) =>
                  setSubmission({ ...submission, content: e.target.value })
                }
                id="message"
                rows="4"
                placeholder="Start your story here..."
              />
            </div>
            <div className="flex items-center justify-end">
              <div className="ml-2">
                <Checkbox
                  id="default-checkbox"
                  checked={anon}
                  onChange={() => setAnon(!anon)}
                  className="inline-block mr-2"
                />
                <Label htmlFor="default-checkbox" className="mr-2">
                  Post Anonymously?
                </Label>
              </div>
              <Button type="submit">Submit</Button>
            </div>
          </form>
        </div>
      </div>

      <div className="py-4">
        <h2 className="text-lg font-bold text-gray-900 lg:text-2xl dark:text-white">
          Today's Stories
        </h2>
        <div className="flex justify-end">
          <div className="py-4 w-72">
            <Listbox value={selectedSort} onChange={setSelectedSort}>
              <div className="relative mt-1">
                <Listbox.Button className="relative w-full py-2 pl-3 pr-10 text-left bg-white rounded-lg shadow-md cursor-default focus:outline-none focus-visible:border-indigo-500 focus-visible:ring-2 focus-visible:ring-white/75 focus-visible:ring-offset-2 focus-visible:ring-offset-orange-300 sm:text-sm">
                  <span className="block truncate">{selectedSort.name}</span>
                  <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                    <ChevronUpDownIcon
                      className="w-5 h-5 text-gray-400"
                      aria-hidden="true"
                    />
                  </span>
                </Listbox.Button>
                <Transition
                  as={Fragment}
                  leave="transition ease-in duration-100"
                  leaveFrom="opacity-100"
                  leaveTo="opacity-0"
                >
                  <Listbox.Options className="absolute w-full py-1 mt-1 overflow-auto text-base bg-white rounded-md shadow-lg max-h-60 ring-1 ring-black/5 focus:outline-none sm:text-sm">
                    {sort.map((sort) => (
                      <Listbox.Option
                        key={sort.id}
                        className={({ active }) =>
                          `relative cursor-default select-none py-2 pl-10 pr-4 ${
                            active
                              ? "bg-amber-100 text-amber-900"
                              : "text-gray-900"
                          }`
                        }
                        value={sort}
                      >
                        {({ selectedSort }) => (
                          <>
                            <span
                              className={`block truncate ${
                                selectedSort ? "font-medium" : "font-normal"
                              }`}
                            >
                              {sort.name}
                            </span>
                            {selectedSort ? (
                              <span className="absolute inset-y-0 left-0 flex items-center pl-3 text-amber-600">
                                <CheckIcon
                                  className="w-5 h-5"
                                  aria-hidden="true"
                                />
                              </span>
                            ) : null}
                          </>
                        )}
                      </Listbox.Option>
                    ))}
                  </Listbox.Options>
                </Transition>
              </div>
            </Listbox>
          </div>
        </div>
        <ul className="p-5 divide-y shadow-lg rounded-xl divide-slate-400 bg-slate-50">
          {currentSubmissions &&
            currentSubmissions.map((submission) => (
              <SubmissionThread
                key={submission.id}
                submission={submission}
                likeStates={likeStates}
                dislikeStates={dislikeStates}
                user_id={userData?.id}
                socket={socket}
              />
            ))}
        </ul>
        <div className="flex overflow-x-auto sm:justify-center">
          <Pagination
            currentPage={currentSubmissionPage}
            totalPages={submissionPages}
            onPageChange={onSubmissionPageChange}
          />
        </div>
      </div>
    </div>
  );
};
export default Main;
