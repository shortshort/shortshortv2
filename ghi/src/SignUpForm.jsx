import { useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { Button, Card, Label, TextInput } from "flowbite-react";
import { useNavigate, NavLink } from "react-router-dom";

const SignupForm = () => {
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const { login } = useToken();
  const [errorMessage, setErrorMessage] = useState();
  const navigate = useNavigate();

  const handleRegistration = async (e) => {
    e.preventDefault();
    const accountData = {
      email: email,
      username: username,
      password: password,
    };
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_HOST}/api/users`,
        {
          method: "POST",
          credentials: "include",
          body: JSON.stringify(accountData),
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      if (!response.ok) {
        setErrorMessage("Couldn't create account, please try a new username.");
        return;
      }
      await login(accountData.username, accountData.password);
      e.target.reset();
      navigate("/");
    } catch (e) {
      setErrorMessage(
        "Couldn't create account, please try a new username or email address"
      );
    }
  };

  return (
    <div className="flex items-center justify-center h-screen">
      <Card className="bg-gray-50" style={{ width: "400px" }}>
        <form
          onSubmit={(e) => handleRegistration(e)}
          className="flex flex-col gap-4"
        >
          {errorMessage ? <p>{errorMessage}</p> : ""}
          <div className="block mb-2">
            <h1>Sign up</h1>
          </div>
          <div>
            <div className="block mb-2">
              <Label htmlFor="email" value="email" />
            </div>
            <TextInput
              id="email"
              type="text"
              placeholder="email@email.com"
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </div>
          <div>
            <div className="block mb-2">
              <Label htmlFor="username" value="username" />
            </div>
            <TextInput
              id="username"
              type="text"
              placeholder="Your Username"
              onChange={(e) => setUsername(e.target.value)}
              required
            />
          </div>
          <div>
            <div className="block mb-2">
              <Label htmlFor="password" value="password" />
            </div>
            <TextInput
              id="password"
              type="password"
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </div>
          <Button type="submit">Submit</Button>
          <div>
            <NavLink to="/login" className="text-blue-50">
              Already have an account? Click here to login.
            </NavLink>
          </div>
        </form>
      </Card>
    </div>
  );
};

export default SignupForm;
