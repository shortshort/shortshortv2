// wanted a centralized location for queries we use in several places

export async function fetchSubmissions() {
  const response = await fetch(
    `${process.env.REACT_APP_API_HOST}/api/submissions`,
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
  if (response.ok) {
    const data = await response.json();
    return data;
  } else {
    throw new Error("Error fetching submissions. Status:", response.status);
  }
}
export async function fetchUserSubmissions(id) {
  const url = `${process.env.REACT_APP_API_HOST}/api/${id}/submissions`;
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();
    return data;
  } else {
    throw new Error(
      "Error fetching user submissions. Status:",
      response.status
    );
  }
}

export async function fetchOneSubmission(id) {
  const url = `${process.env.REACT_APP_API_HOST}/api/submissions/${id}`;
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();
    return data;
  } else {
    throw new Error("Error fetching submission. Status:", response.status);
  }
}

export async function fetchUser(user_id) {
  const url = `${process.env.REACT_APP_API_HOST}/api/users/${user_id}`;
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();
    return data;
  } else {
    throw new Error("Error fetching user. Status:", response.status);
  }
}

export async function fetchPrompts() {
  const response = await fetch(
    `${process.env.REACT_APP_API_HOST}/api/prompts`,
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
  if (response.ok) {
    const data = await response.json();
    return data;
  } else {
    throw new Error("Error fetching prompts. Status:", response.status);
  }
}

export async function fetchOnePrompt(count) {
  const url = `${process.env.REACT_APP_API_HOST}/api/prompts/${count}`;
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();
    return data;
  } else {
    throw new Error("Error fetching prompt. Status:", response.status);
  }
}

export async function fetchLikes(userData) {
  if (userData) {
    const url = `${process.env.REACT_APP_API_HOST}/api/${userData.id}/likes`;
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      return data;
    } else {
      throw new Error("Error fetching likes. Status:", response.status);
    }
  }
}

export async function fetchDislikes(userData) {
  if (userData) {
    const url = `${process.env.REACT_APP_API_HOST}/api/${userData.id}/dislikes`;
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      return data;
    } else {
      throw new Error("Error fetching dislikes. Status:", response.status);
    }
  }
}

export async function fetchComments(id) {
  const url = `${process.env.REACT_APP_API_HOST}/api/submissions/${id}/comments/`;
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();
    return data;
  } else {
    throw new Error("Error fetching comments. Status:", response.status);
  }
}

export async function fetchUserComments(id) {
  const url = `${process.env.REACT_APP_API_HOST}/api/${id}/comments`;
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();
    return data;
  } else {
    throw new Error("Error fetching user comments. Status:", response.status);
  }
}

export async function updateSubmission(data) {
  const { id, ...rest } = data;
  const url = `${process.env.REACT_APP_API_HOST}/api/submissions/${id}`;

  const response = await fetch(url, {
    method: "PUT",
    body: JSON.stringify(rest),
    headers: {
      "Content-Type": "application/json",
    },
  });
  if (response.ok) {
    const data = await response.json();
    return data;
  } else {
    return { error: response.text };
  }
}
