import { Navbar, Avatar, Dropdown,  Modal } from "flowbite-react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { useNavigate, Link, useLocation } from "react-router-dom";
import React, { useState } from "react";
import { HiQuestionMarkCircle, HiBookOpen, HiOutlineBookOpen } from "react-icons/hi";
import "./styles.css"



export default function Navigationbar({ userData }) {
  const [openModal, setOpenModal] = useState(false);
  const { logout } = useToken();
  const { token } = useAuthContext();
  const navigate = useNavigate();
  const id = userData ? userData.id : null;
  const location = useLocation();
  const isSubmissionsPage = location.pathname === '/submissions';

  function signout() {
    logout();
    navigate("/login");
    window.location.reload();
  }

  return (
    <Navbar
      className="bg-[#f4f2e9] border-solid border-zinc-600 border-b-2"
      fluid
      rounded
    >
      <Modal show={openModal} onClose={() => setOpenModal(false)}>
        <Modal.Header>Short Short Stories</Modal.Header>
        <Modal.Body>
          <div className="space-y-6">
            <p className="text-base leading-relaxed text-gray-500 dark:text-gray-400">
              Use the daily writing prompts to write your own "short short" story! Stories have a limit
              of 500 characters, but feel free to write as many stories as you want!
            </p>
            <p className="text-base leading-relaxed text-gray-500 dark:text-gray-400">
              Check out other people's stories and rate and comment to give your opinion!
            </p>
          </div>
        </Modal.Body>
      </Modal>
      <div className="flex justify-between items-center w-full">
        <button
        onClick={() => setOpenModal(true)}
        className="book-open-link"
      >
        <HiQuestionMarkCircle className="icon" />
        <span className="tooltip">Info</span>
      </button>
      <Link to="submissions" className="book-open-link m-2">
      {isSubmissionsPage ? (
        <HiOutlineBookOpen className="icon" />
      ) : (
        <HiBookOpen className="icon" />
      )}
      <span className="tooltip">Past Stories</span>
    </Link>
        <div className="flex-1"></div>
        <Navbar.Brand as={Link} to="/" className="flex-1 flex justify-center autour-one-regular ">
          Short Short Stories
        </Navbar.Brand>
        <div className="flex-1 flex justify-end items-center md:order-2">
          <Dropdown
            arrowIcon={false}
            inline
            label={
              <Avatar 
                alt="User Avatar" 
                img={userData?.avatar_picture || "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Default_pfp.svg/2048px-Default_pfp.svg.png"} 
                rounded 
              />
            }
          >
            <Dropdown.Header>
              <span className="block text-sm">
        {userData?.username || (
          <>
            <Link to="/login" className="text-blue-500">Log In</Link> or <Link to="/signup" className="text-blue-500">Sign Up</Link>
          </>
        )}
      </span>
              <span className="block truncate text-sm font-medium">{userData?.email || null}</span>
            </Dropdown.Header>
          {userData && (
      <>
        <Dropdown.Item as={Link} to={`/users/${userData.id}`}>Profile</Dropdown.Item>
        <Dropdown.Divider />
        <Dropdown.Item onClick={signout}>Sign out</Dropdown.Item>
      </>
    )}
          </Dropdown>
          <Navbar.Toggle />
        </div>
      </div>
    </Navbar>
  );
}
