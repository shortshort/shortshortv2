import { Combobox, Transition, Listbox } from "@headlessui/react";
import { useEffect, useState, Fragment } from "react";
import SubmissionThread from "./SubmissionThread";
import { HiChevronDown, HiCheck } from "react-icons/hi";
import { Pagination } from "flowbite-react";
import { CheckIcon, ChevronUpDownIcon } from "@heroicons/react/20/solid";
import {
    fetchLikes,
    fetchDislikes,
    fetchSubmissions,
    fetchPrompts,
} from "./actions/queries";

export default function SubmissionList({ userData, socket }) {
    const [prompts, setPrompts] = useState([]);
    const [submissions, setSubmissions] = useState([]);
    const [selected, setSelected] = useState({});
    const [query, setQuery] = useState("");
    const [likeStates, setLikeStates] = useState({});
    const [dislikeStates, setDislikeStates] = useState({});
    const sort = [
        { id: 1, name: "Latest" },
        { id: 2, name: "Oldest" },
        { id: 3, name: "Most Popular" },
        { id: 4, name: "Least Popular" },
    ];
    const [selectedSort, setSelectedSort] = useState(sort[0]);
    const [currentSubmissionPage, setCurrentSubmissionPage] = useState(1);
    const [submissionsPerPage, setSubmissionsPerPage] = useState(10);
    const [currentSubmissions, setCurrentSubmissions] = useState([]);

    // Sorting

    function sortSubmissions(submissions, sort) {
        switch (sort.id) {
            case 1:
                return submissions.sort(
                    (a, b) => new Date(b.created_at) - new Date(a.created_at)
                );
            case 2:
                return submissions.sort(
                    (a, b) => new Date(a.created_at) - new Date(b.created_at)
                );
            case 3:
                return submissions.sort((a, b) => b.likes - a.likes);
            case 4:
                return submissions.sort((a, b) => a.likes - b.likes);
            default:
                return submissions;
        }
    }

    // Pagination
    const onSubmissionPageChange = (page) => setCurrentSubmissionPage(page);
    const submissionPages = Math.ceil(submissions?.length / submissionsPerPage);
    const indexOfLastSubmission = currentSubmissionPage * submissionsPerPage;
    const indexOfFirstSubmission = indexOfLastSubmission - submissionsPerPage;

    // Filters prompts based on what the user types in the search bar
    const filteredPrompts =
        query === ""
            ? prompts
            : prompts.filter((prompt) =>
                  prompt.content
                      .toLowerCase()
                      .replace(/\s+/g, "")
                      .includes(query.toLowerCase().replace(/\s+/g, ""))
              );

    // fetch submissions and prompts
    useEffect(() => {
        const fetchData = async () => {
            const submissions = await fetchSubmissions();
            setSubmissions(sortSubmissions(submissions, selectedSort));
            setCurrentSubmissions(
                submissions.slice(indexOfFirstSubmission, indexOfLastSubmission)
            );

            const prompts = await fetchPrompts();
            setPrompts(prompts.prompts);
            setSelected(prompts.prompts[0]);
        };
        fetchData();
        // eslint-disable-next-line
    }, []);

    //fetches likes and dislikes for the specific user
    // only fetches if user is authenticated
    useEffect(() => {
        const fetchData = async () => {
            if (userData) {
                const likes = await fetchLikes(userData);
                const dislikes = await fetchDislikes(userData);
                setLikeStates(likes);
                setDislikeStates(dislikes);
            }
        };
        fetchData();
        // eslint-disable-next-line
    }, [userData]);

    return (
        <div className="mx-5 mt-5">
            <div className="flex justify-between w-full mb-2">
                <Combobox value={selected} onChange={setSelected}>
                    <div className="relative mt-1">
                        <div className="relative w-full overflow-hidden text-left bg-white rounded-lg shadow-md cursor-default focus:outline-none focus-visible:ring-2 focus-visible:ring-white/75 focus-visible:ring-offset-2 focus-visible:ring-offset-teal-300 sm:text-sm">
                            <Combobox.Input
                                className="w-full border-0 rounded-lg"
                                onChange={(e) => setQuery(e.target.value)}
                                displayValue={(selected) => selected.content}
                            />
                            <Combobox.Button className="absolute right-0 transform -translate-y-1/2 top-1/2">
                                <HiChevronDown
                                    className="w-5 h-5 text-gray-400"
                                    aria-hidden="true"
                                />
                            </Combobox.Button>
                        </div>
                        <Transition
                            as={Fragment}
                            leave="transition ease-in duration-100"
                            leaveFrom="opacity-100"
                            leaveTo="opacity-0"
                            afterLeave={() => setQuery("")}
                        >
                            <Combobox.Options className="absolute w-full py-1 mt-1 overflow-auto text-base bg-white rounded-md shadow-lg max-h-60 ring-1 ring-black/5 focus:outline-none sm:text-sm">
                                {filteredPrompts.length === 0 &&
                                query !== "" ? (
                                    <div>Nothing Found</div>
                                ) : (
                                    filteredPrompts.map((prompt) => (
                                        <Combobox.Option
                                            key={prompt.id}
                                            value={prompt}
                                            className={({ active }) =>
                                                `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                                    active
                                                        ? "bg-teal-600 text-white"
                                                        : "text-gray-900"
                                                }`
                                            }
                                            onSelect={(value) =>
                                                setSelected(value)
                                            }
                                        >
                                            {({ selected, active }) => (
                                                <>
                                                    <span
                                                        className={`block truncate ${
                                                            active
                                                                ? "text-white"
                                                                : "text-gray-900"
                                                        }`}
                                                    >
                                                        {prompt.content}
                                                    </span>
                                                    {selected ? (
                                                        <span
                                                            className={`absolute inset-y-0 left-0 flex items-center pl-3 ${
                                                                active
                                                                    ? "text-white"
                                                                    : "text-indigo-600"
                                                            }`}
                                                        >
                                                            <HiCheck
                                                                className="w-5 h-5"
                                                                aria-hidden="true"
                                                            />
                                                        </span>
                                                    ) : null}
                                                </>
                                            )}
                                        </Combobox.Option>
                                    ))
                                )}
                            </Combobox.Options>
                        </Transition>
                    </div>
                </Combobox>
                <Listbox
                    value={selectedSort}
                    onChange={setSelectedSort}
                    className="w-52"
                >
                    <div className="relative mt-1">
                        <Listbox.Button className="relative w-full py-2 pl-3 pr-10 text-left bg-white rounded-lg shadow-md cursor-default focus:outline-none focus-visible:border-indigo-500 focus-visible:ring-2 focus-visible:ring-white/75 focus-visible:ring-offset-2 focus-visible:ring-offset-orange-300 sm:text-sm">
                            <span className="block truncate">
                                {selectedSort.name}
                            </span>
                            <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                                <ChevronUpDownIcon
                                    className="w-5 h-5 text-gray-400"
                                    aria-hidden="true"
                                />
                            </span>
                        </Listbox.Button>
                        <Transition
                            as={Fragment}
                            leave="transition ease-in duration-100"
                            leaveFrom="opacity-100"
                            leaveTo="opacity-0"
                        >
                            <Listbox.Options className="absolute w-full py-1 mt-1 overflow-auto text-base bg-white rounded-md shadow-lg max-h-60 ring-1 ring-black/5 focus:outline-none sm:text-sm">
                                {sort.map((sort) => (
                                    <Listbox.Option
                                        key={sort.id}
                                        className={({ active }) =>
                                            `relative cursor-default select-none py-2 pl-10 pr-4 ${
                                                active
                                                    ? "bg-amber-100 text-amber-900"
                                                    : "text-gray-900"
                                            }`
                                        }
                                        value={sort}
                                    >
                                        {({ selectedSort }) => (
                                            <>
                                                <span
                                                    className={`block truncate ${
                                                        selectedSort
                                                            ? "font-medium"
                                                            : "font-normal"
                                                    }`}
                                                >
                                                    {sort.name}
                                                </span>
                                                {selectedSort ? (
                                                    <span className="absolute inset-y-0 left-0 flex items-center pl-3 text-amber-600">
                                                        <CheckIcon
                                                            className="w-5 h-5"
                                                            aria-hidden="true"
                                                        />
                                                    </span>
                                                ) : null}
                                            </>
                                        )}
                                    </Listbox.Option>
                                ))}
                            </Listbox.Options>
                        </Transition>
                    </div>
                </Listbox>
            </div>
            <ul className="p-5 divide-y shadow-lg rounded-xl divide-slate-400 bg-slate-50">
                {currentSubmissions
                    .filter(
                        (submission) => submission.prompt_id === selected.id
                    )
                    .map((submission) => (
                        <SubmissionThread
                            key={submission.id}
                            likes={submission.likes}
                            dislikes={submission.dislikes}
                            submission={submission}
                            user_id={userData?.id}
                            socket={socket}
                            likeStates={likeStates}
                            dislikeStates={dislikeStates}
                        />
                    ))}
            </ul>
            <div className="flex overflow-x-auto sm:justify-center">
                <Pagination
                    currentPage={currentSubmissionPage}
                    totalPages={submissionPages}
                    onPageChange={onSubmissionPageChange}
                />
            </div>
        </div>
    );
}
