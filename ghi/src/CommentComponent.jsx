import React, { useEffect } from "react";
import { useState } from "react";
import LikeDislikeComponent from "./LikeDislikeComponent";
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";


export default function CommentComponent({ comment, likeStates, dislikeStates, user_id}) {

    const [user, setUser] = useState({});
    const [likes, setLikes] = useState(comment.likes);
    const [dislikes, setDislikes] = useState(comment.dislikes);
    const [socket, setSocket] = useState(null);

    dayjs.extend(relativeTime);


    async function fetchUser() {
        const url = `${process.env.REACT_APP_API_HOST}/api/users/${comment.user_id}`;
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();
        setUser(data);
        }
    }

    useEffect(() => {
        fetchUser();
    }, []);

// Websocket logic
  useEffect(() => {
    const socket = new WebSocket("ws://localhost:8000/ws/comments");
    setSocket(socket);

    return () => {
      if (socket) {
        socket.close();
      }
    };
  }, []);



return (
  <>
    <div className="flex justify-between py-5 gap-x-1">
        <div className="flex min-w-0 space-x-2 gap-x-4">
      <LikeDislikeComponent
        likes={likes}
        dislikes={dislikes}
        likeStates={likeStates}
        dislikeStates={dislikeStates}
        user_id={user_id}
        comment={comment}
        socket={socket}
        userData={user}
      />
      <div className="flex items-center shrink-0">
        <img
          src={user.avatar_picture}
          className="object-cover w-24 h-24 rounded-full shadow-md"
          alt="User Avatar"
        />
      </div>
      <div className="flex-auto min-w-0">
        <div className="flex items-center justify-between">
          <h2 className="text-lg font-semibold">{user.username}</h2>
          &nbsp;
          <p className="text-sm leading-6 text-gray-500">{dayjs(comment.created_at).fromNow()}</p>
        </div>
        <p className="h-20 mt-1 overflow-auto leading-5 text-gray-500 max-w-screen-2xl text-md">
            {comment.content}
        </p>
        </div>
        </div>
    </div>
  </>
);
}
