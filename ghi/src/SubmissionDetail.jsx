import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";
import CommentComponent from "./CommentComponent";
import LikeDislikeComponent from "./LikeDislikeComponent";
import { Button } from "flowbite-react";
import {
    fetchOneSubmission,
    fetchComments,
    fetchLikes,
    fetchDislikes,
    fetchOnePrompt,
    fetchUser,
    updateSubmission,
} from "./actions/queries";

export default function SubmissionDetail({
    currentDateTime,
    userData,
    socket,
}) {
    const [submission, setSubmission] = useState();
    const [loading, setLoading] = useState(true);
    const [poster, setPoster] = useState({});
    const [prompt, setPrompt] = useState({});
    const [comments, setComments] = useState();
    const [comment, setComment] = useState();
    const { id } = useParams();
    const [edit, setEdit] = useState(false);
    const [submissionEdit, setSubmissionEdit] = useState({});

    // likes and dislikes logic
    const [likes, setLikes] = useState(0);
    const [dislikes, setDislikes] = useState(0);
    const [likeStates, setLikeStates] = useState({});
    const [dislikeStates, setDislikeStates] = useState({});

    //fetches likes and dislikes for the specific user
    // only fetches if user is authenticated
    useEffect(() => {
        const fetchData = async () => {
            if (userData) {
                const likes = await fetchLikes(userData);
                const dislikes = await fetchDislikes(userData);
                setLikeStates(likes);
                setDislikeStates(dislikes);
            }
        };
        fetchData();
        // eslint-disable-next-line
    }, [userData]);

    useEffect(() => {
        const fetchData = async () => {
            const submission = await fetchOneSubmission(id);
            const comments = await fetchComments(id);
            setLoading(false);
            setSubmission(submission);
            setSubmissionEdit(submission);
            if (submission.anon_flag) {
                setPoster(null);
            } else {
                const poster = await fetchUser(submission.user_id);
                setPoster(poster);
            }

            const prompt = await fetchOnePrompt(submission.prompt_id);
            setPrompt(prompt);

            setLikes(submission.likes);
            setDislikes(submission.dislikes);
            setComments(comments);
        };
        fetchData();
        // eslint-disable-next-line
    }, [id]);

    const postComment = async (e) => {
        e.preventDefault();
        const commentData = {
            user_id: userData.id,
            submission_id: comment.submission_id,
            content: comment.content,
            created_at: currentDateTime,
            likes: 0,
            dislikes: 0,
        };

        try {
            const response = await fetch(
                `${process.env.REACT_APP_API_HOST}/api/submissions/${comment.submission_id}/comments/`,
                {
                    method: "POST",
                    body: JSON.stringify(commentData),
                    headers: {
                        "Content-Type": "application/json",
                    },
                }
            );
            if (response.ok) {
                console.log("Successfully posted comment");
            } else {
                console.error("Failed to post comment:", response.statusText);
            }
        } catch (error) {
            console.error("Error:");
        }
    };
    if (loading) {
        return <h1>Loading...</h1>;
    }

    // editing submission logic
    const handleEdit = (e) => {
        setSubmissionEdit({
            ...submissionEdit,
            [e.target.name]: e.target.value,
        });
    };

    const handleEditSubmit = async (e) => {
        e.preventDefault();

        const response = await updateSubmission(submissionEdit);
        if (!response.error) {
            console.log("Successfully updated submission");
            window.location.reload();
        } else {
            console.error("Failed to update submission:", response.error);
        }
    };

    return (
        <>
            <div className="gap-4 mx-20 mt-4">
                <div className="flex justify-between col-span-4 row-span-1 gap-2 p-2 mb-4 rounded-lg shadow-lg bg-slate-50">
                    <div className="flex flex-col gap-2 p-10">
                        {submission && edit ? (
                            <>
                                <label htmlFor="title">New Title</label>
                                <input
                                    onChange={handleEdit}
                                    id="title"
                                    name="title"
                                    value={submissionEdit.title}
                                    className="p-1 rounded-lg shadow-lg"
                                ></input>
                            </>
                        ) : (
                            <h1 className="text-6xl bold">
                                {submission?.title}
                            </h1>
                        )}

                        <p className="text-lg">
                            by{" "}
                            {submission.anon_flag ? (
                                "Anonymous"
                            ) : (
                                <Link
                                    to={`/users/${poster.id}`}
                                    className="hover:underline hover:text-blue-500"
                                >
                                    {poster.username}
                                </Link>
                            )}
                        </p>
                        <p className="italic">{prompt.content}</p>
                        {userData && userData.id === submission.user_id && (
                            <>
                                <Button
                                    onClick={() => setEdit(!edit)}
                                    className="w-20 text-nowrap"
                                >
                                    Edit Post
                                </Button>
                                {edit ? (
                                    <Button
                                        onClick={handleEditSubmit}
                                        className="w-32 text-nowrap"
                                    >
                                        Submit Changes
                                    </Button>
                                ) : null}
                            </>
                        )}
                    </div>
                    <div className="flex items-center justify-center mr-5">
                        {submission.anon_flag ? (
                            <img
                                alt="Profile"
                                src="https://merriam-webster.com/assets/mw/images/article/art-wap-article-main/egg-3442-e1f6463624338504cd021bf23aef8441@1x.jpg"
                                className="rounded-full w-36 h-36"
                            />
                        ) : (
                            <Link to={`/users/${poster.id}`}>
                                <img
                                    alt="Profile"
                                    src={poster.avatar_picture}
                                    className="rounded-full w-36 h-36"
                                />
                            </Link>
                        )}
                    </div>
                </div>
                <div className="relative flex flex-row p-5 divide-x rounded-lg shadow-lg divide-slate-400 bg-slate-50">
                    <div className="flex flex-col justify-center gap-5 mx-1">
                        <LikeDislikeComponent
                            likes={likes}
                            dislikes={dislikes}
                            submission={submission}
                            user_id={userData?.id}
                            socket={socket}
                            likeStates={likeStates}
                            dislikeStates={dislikeStates}
                        />
                    </div>
                    <div className="pl-12 mx-8 my-6">
                        {submission && edit ? (
                            <div className="flex flex-col w-full">
                                <label>edit your submission</label>
                                <textarea
                                    className="w-full rounded"
                                    onChange={handleEdit}
                                    name="content"
                                    value={submissionEdit.content}
                                    rows={6}
                                    cols={150}
                                ></textarea>
                            </div>
                        ) : (
                            <p className="justify-center mb-5">
                                {submission?.content}
                            </p>
                        )}

                        <p className="absolute bottom-0 right-0 self-end mt-4 mb-4 mr-4 italic text-right">
                            Posted on:{" "}
                            {new Date(submission?.created_at)
                                .toLocaleString("en-US", {
                                    month: "long",
                                    day: "numeric",
                                    year: "numeric",
                                })
                                .replace(/(\d{1,2})(st|nd|rd|th)/, "$1$2,")}
                        </p>
                    </div>
                </div>
            </div>
            <div className="mx-20 mt-4 ">
                <form className="mb-1" onSubmit={(e) => postComment(e)}>
                    <div className="px-4 py-2 mb-4 bg-white border border-gray-200 rounded-lg rounded-t-lg dark:bg-gray-800 dark:border-gray-700">
                        <label htmlFor="comment" className="sr-only">
                            Your comment
                        </label>
                        <textarea
                            id="comment"
                            rows="1"
                            onChange={(e) =>
                                setComment({
                                    ...comment,
                                    content: e.target.value,
                                    submission_id: submission?.id,
                                })
                            }
                            className="w-full px-0 text-sm text-gray-900 border-0 focus:ring-0 focus:outline-none dark:text-white dark:placeholder-gray-400 dark:bg-gray-800"
                            placeholder="Write a comment..."
                            required
                        ></textarea>
                    </div>
                    <div className="flex items-center justify-end">
                        <Button type="submit">Post comment</Button>
                    </div>
                </form>
            </div>
            <div>
                <h2 className="mx-20 text-lg font-bold text-gray-900 lg:text-2xl dark:text-white">
                    Comments
                </h2>
                <ul className="p-5 mx-20 mt-4 divide-y shadow-lg rounded-xl divide-slate-400 bg-slate-50">
                    {comments?.map((comment) => (
                        <CommentComponent
                            key={comment.id}
                            comment={comment}
                            likeStates={likeStates}
                            dislikeStates={dislikeStates}
                            user_id={userData?.id}
                        />
                    ))}
                </ul>
            </div>
        </>
    );
}
