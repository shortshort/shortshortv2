import React, { useEffect, useState } from "react";
import {
  HiOutlineThumbUp,
  HiOutlineThumbDown,
  HiThumbUp,
  HiThumbDown,
} from "react-icons/hi";
import LoginModal from "./LoginModal";
import InteractionButton from "./InteractionButton";

export default function LikeDislikeComponent({
  likes,
  dislikes,
  likeStates,
  dislikeStates,
  user_id,
  submission = null,
  comment = null,
  socket,
  userData,
}) {
  const [openModal, setOpenModal] = useState(false);
  const [interactionState, setInteractionState] = useState({
    like: false,
    dislike: false,
  });
  const [currLikes, setCurrLikes] = useState(likes);
  const [currDislikes, setCurrDislikes] = useState(dislikes);
  useEffect(() => {
    setCurrLikes(likes);
    setCurrDislikes(dislikes);
  }, [likes, dislikes]);

  const onCloseModal = () => {
    setOpenModal(false);
  };

  const onLoginSuccess = () => {
    setOpenModal(false);
  };

  // Sets the like and dislike state of the component
  const updateInteractionState = (id, key) => {
    const liked = Object.values(likeStates).some((state) => state[key] === id);
    const disliked = Object.values(dislikeStates).some(
      (state) => state[key] === id
    );
    setInteractionState({ like: liked, dislike: disliked });
  };

  useEffect(() => {
    if (submission) {
      updateInteractionState(submission.id, "submission_id");
    } else if (comment) {
      updateInteractionState(comment.id, "comment_id");
    }
  }, [likeStates, dislikeStates, submission, comment]);

  // Like and Dislike Logic (can unlike and undislike as well)
  const sendMessage = (message) => {
    if (socket) {
      let payload = { message, user_id: user_id };
      if (submission) {
        payload.submission = submission;
      } else if (comment) {
        payload.comment = comment;
      }
      socket.send(JSON.stringify(payload));
    }
  };

  const handleInteraction = (interaction) => {
    if (!user_id) {
      setOpenModal(true);
      return;
    }
    if (interaction === "like" && interactionState.dislike) {
      setInteractionState({ like: true, dislike: false });
      sendMessage("undislike");
      sendMessage("like");
    } else if (interaction === "dislike" && interactionState.like) {
      setInteractionState({ like: false, dislike: true });
      sendMessage("unlike");
      sendMessage("dislike");
    } else if (interaction === "like") {
      setInteractionState({ like: true, dislike: false });
      console.log("like");
      sendMessage("like");
    } else if (interaction === "unlike") {
      setInteractionState({ like: false, dislike: false });
      console.log("unlike");
      sendMessage("unlike");
    } else if (interaction === "dislike") {
      setInteractionState({ like: false, dislike: true });
      console.log("dislike");
      sendMessage("dislike");
    } else if (interaction === "undislike") {
      setInteractionState({ like: false, dislike: false });
      console.log("undislike");
      sendMessage("undislike");
    }
  };

  const sendLike = () => {
    handleInteraction("like");
  };

  const sendUnlike = () => {
    handleInteraction("unlike");
  };

  const sendDislike = () => {
    handleInteraction("dislike");
  };

  const sendUndislike = () => {
    handleInteraction("undislike");
  };

  // receiving messages from the websocket logic
  const handleMessage = (event) => {
    try {
      const message = JSON.parse(event.data);
      const updatedLikes = message.likes;
      const updatedDislikes = message.dislikes;
      setCurrLikes(updatedLikes);
      setCurrDislikes(updatedDislikes);
    } catch (error) {
      console.log("error", error);
    }
  };
  useEffect(() => {
    if (socket && socket.readyState === WebSocket.OPEN) {
      socket.addEventListener("message", handleMessage);
    }
    return () => {
      if (socket) {
        socket.removeEventListener("message", handleMessage);
      }
    };
  }, [socket]);

  return (
    <>
      <LoginModal
        isOpen={openModal}
        onClose={onCloseModal}
        onLoginSuccess={onLoginSuccess}
      />

      <div className="flex flex-col justify-center gap-1">
        <InteractionButton
          active={interactionState.like}
          onClick={interactionState.like ? sendUnlike : sendLike}
          IconActive={HiThumbUp}
          IconInactive={HiOutlineThumbUp}
          className="cursor-pointer"
        />
        <div className="justify-center">
          <p className="font-semibold text-center text-green-500">
            {currLikes}
          </p>
          <p className="font-semibold text-center text-red-500">
            {currDislikes}
          </p>
        </div>
        <InteractionButton
          active={interactionState.dislike}
          onClick={interactionState.dislike ? sendUndislike : sendDislike}
          IconActive={HiThumbDown}
          IconInactive={HiOutlineThumbDown}
          className="cursor-pointer"
        />
      </div>
    </>
  );
}
