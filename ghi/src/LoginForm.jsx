import useToken from "@galvanize-inc/jwtdown-for-react";
import { HiInformationCircle } from "react-icons/hi";
import { Button, Card, Label, TextInput, Alert } from "flowbite-react";
import { useState, useEffect } from "react";
import { useNavigate, NavLink } from "react-router-dom";

const LoginForm = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const { login, token } = useToken();
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();
    setError("");
    await login(username, password);

    setTimeout(() => {
      if (!token) {
        setError(" Invalid username or password!");
      }
    }, 1000);
  };

  useEffect(() => {
    if (token) {
      navigate("/");
    }
  }, [token, navigate]);

  return (
    <div className="flex items-center justify-center h-screen">
      <Card className="bg-gray-50" style={{ width: "400px" }}>
        {error && (
          <Alert color="failure" icon={HiInformationCircle}>
            <span className="font-medium">Info alert!</span>
            {error}
          </Alert>
        )}
        <form onSubmit={handleSubmit} className="flex flex-col gap-4">
          <div className="block mb-2">
            <h1>Login</h1>
          </div>
          <div>
            <div className="block mb-2">
              <Label htmlFor="username" value="username" />
            </div>
            <TextInput
              id="username"
              type="text"
              placeholder="Your username"
              autoComplete="username"
              onChange={(e) => setUsername(e.target.value)}
              required
            />
          </div>
          <div>
            <div className="block mb-2">
              <Label htmlFor="password" value="password" />
            </div>
            <TextInput
              id="password"
              type="password"
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </div>
          <Button type="submit">Submit</Button>
          <div>
            <NavLink to="/signup" className="text-blue-50">
              Don't have an account? Click here to create a new account.
            </NavLink>
          </div>
        </form>
      </Card>
    </div>
  );
};

export default LoginForm;
