

export default function InteractionButton({ active, onClick, IconActive, IconInactive }) {

    const Icon = active ? IconActive : IconInactive;

    return (
        <Icon
        onClick={onClick}
        className="cursor-pointer"
        />
  );
}
