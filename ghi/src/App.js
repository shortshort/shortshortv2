import SignupForm from "./SignUpForm.jsx";
import LoginForm from "./LoginForm.jsx";
import Main from "./Main.jsx";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Navigationbar from "./Navbar.jsx";
import SubmissionDetail from "./SubmissionDetail.jsx";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import React, { useEffect, useState } from "react";
import SubmissionList from "./SubmissionList.jsx";
import UserDetail from "./UserDetail.jsx";
import dayjs from "dayjs";

function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");

  const { token } = useAuthContext();
  const [userData, setUserData] = useState(null);
  const [socket, setSocket] = useState(null);

  // Date Conversion

  const currentDateTime = new Date();

  // Fetch User Data from Token
  async function fetchData() {
    if (token) {
      const tokenUrl = `${process.env.REACT_APP_API_HOST}/token`;
      try {
        const response = await fetch(tokenUrl, {
          headers: { Authorization: `Bearer ${token}` },
          credentials: "include",
        });

        if (response.ok) {
          const data = await response.json();
          setUserData(data.user);
        } else {
          console.error("Error fetching user data. Status:", response.status);
        }
      } catch (error) {
        console.error("Error fetching user data:", error);
      }
    }
  }
  useEffect(() => {
    fetchData();
    // eslint-disable-next-line
  }, [token]);

  // Submission Websocket logic
  useEffect(() => {
    const socket = new WebSocket("ws://localhost:8000/ws/submissions");
    setSocket(socket);

    // Error handling
    socket.onerror = (error) => {
      console.error("WebSocket error:", error);
    };

    //  Close handling
    socket.onclose = (event) => {
      console.log("WebSocket is closed now.", event);
    };

    return () => {
      if (socket && socket.readyState === WebSocket.OPEN) {
        socket.close();
      }
    };
  }, []);

  return (
    <BrowserRouter basename={basename}>
      <Navigationbar userData={userData} />
      <Routes>
        <Route
          exact
          path="/"
          element={
            <Main
              token={token}
              userData={userData}
              currentDateTime={currentDateTime}
              socket={socket}
            />
          }
        ></Route>
        <Route exact path="/signup" element={<SignupForm />}></Route>
        <Route exact path="/login" element={<LoginForm />}></Route>
        <Route path="submissions">
          <Route
            index
            element={<SubmissionList userData={userData} socket={socket} />}
          />
          <Route
            path=":id"
            element={
              <SubmissionDetail
                token={token}
                userData={userData}
                currentDateTime={currentDateTime}
                socket={socket}
              />
            }
          />
        </Route>
        <Route exact path="/user" element={<UserDetail />}></Route>
        <Route
          exact
          path="/users/:id"
          element={<UserDetail userData={userData} />}
        ></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
