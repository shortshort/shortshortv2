import React, { useState } from "react";
import { Button, Label, Modal, TextInput } from 'flowbite-react';
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate, NavLink } from "react-router-dom";

export default function LoginModal({ isOpen, onClose, onLoginSuccess }) {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');
    const { login } = useToken();

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            await login(username, password);
            setUsername('');
            setPassword('');
            setError('');
            onLoginSuccess();
        } catch (error) {
            setError("Login failed: Invalid username or password");
            console.error("Login failed:", error);
        }
    };

    return (
        <Modal show={isOpen} size="md" onClose={onClose} popup>
            <Modal.Header />
            <Modal.Body>
                <form onSubmit={handleSubmit}>
                    <div className="space-y-6">
                        <h3 className="text-xl font-medium text-gray-900 dark:text-white">Sign in to our platform</h3>
                        {error && <div className="text-red-500">{error}</div>}
                        <div>
                            <div className="block mb-2">
                                <Label htmlFor="username" value="Your username" />
                            </div>
                            <TextInput
                                id="username"
                                placeholder="username"
                                value={username}
                                onChange={(event) => setUsername(event.target.value)}
                                required
                            />
                        </div>
                        <div>
                            <div className="block mb-2">
                                <Label htmlFor="password" value="Your password" />
                            </div>
                            <TextInput
                                id="password"
                                type="password"
                                value={password}
                                onChange={(event) => setPassword(event.target.value)}
                                required
                            />
                        </div>
                        <div className="w-full">
                            <Button type="submit">Log in to your account</Button>
                        </div>
                        <div className="flex justify-between text-sm font-medium text-gray-500 dark:text-gray-300">
                            Not registered?
                            <NavLink to="/signup" className="text-blue-50">
                                Create account
                            </NavLink>
                        </div>
                    </div>
                </form>
            </Modal.Body>
        </Modal>
    );
}
