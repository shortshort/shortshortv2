import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import LikeDislikeComponent from "./LikeDislikeComponent";
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";

export default function SubmissionThread({
  likeStates,
  dislikeStates,
  submission,
  user_id,
}) {
  const [likes, setLikes] = useState(submission.likes);
  const [dislikes, setDislikes] = useState(submission.dislikes);
  const [socket, setSocket] = useState(null);
  const [submissionUser, setSubmissionUser] = useState(null);

  dayjs.extend(relativeTime);

  // Fetches the user data for the submission
  async function fetchSubmissionUser() {
    const url = `${process.env.REACT_APP_API_HOST}/api/users/${submission.user_id}`;
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSubmissionUser(data);
    }
  }

  // Websocket logic
  useEffect(() => {
    const socket = new WebSocket("ws://localhost:8000/ws/submissions");
    setSocket(socket);

    return () => {
      if (socket) {
        socket.close();
      }
    };
  }, []);

  // Fetches the user data for the submission
  useEffect(() => {
    if (submission.anon_flag === true) {
      setSubmissionUser(null);
    } else if (submission.user_id) {
      fetchSubmissionUser();
    } else {
      setSubmissionUser(null);
    }
  }, [submission.user_id]);

  // Websocket logic
  useEffect(() => {
    if (socket) {
      socket.onmessage = (event) => {
        const data = JSON.parse(event.data);
        if (data.id === submission.id) {
          setLikes(data.likes);
          setDislikes(data.dislikes);
        }
      };
    }
  }, [socket, submission.id, likes, dislikes]);

  return (
    <>
      <div className="flex justify-between py-5 gap-x-1">
        <div className="flex min-w-0 space-x-2 gap-x-4">
          <LikeDislikeComponent
            likes={likes}
            dislikes={dislikes}
            submission={submission}
            user_id={user_id}
            socket={socket}
            likeStates={likeStates}
            dislikeStates={dislikeStates}
          />
          <div className="flex items-center shrink-0">
            <img
              className="object-cover w-24 h-24 rounded-full shadow-md"
              src={
                submissionUser
                  ? submissionUser.avatar_picture
                  : "https://merriam-webster.com/assets/mw/images/article/art-wap-article-main/egg-3442-e1f6463624338504cd021bf23aef8441@1x.jpg"
              }
              alt="user avatar"
            />
          </div>
          <div className="flex-auto min-w-0">
            <p className="text-sm font-semibold leading-6 text-gray-900">
              <Link
                to={`/submissions/${submission.id}`}
                className="hover:underline"
              >
                {submission.title}
              </Link>
              <span> by </span>
              {submission.anon_flag ? (
                "Anonymous"
              ) : (
                <Link
                  to={`/users/${submission.user_id}`}
                  className="hover:underline"
                >
                  {submissionUser ? ` ${submissionUser.username}` : "Anonymous"}
                </Link>
              )}
            </p>
            <p className="h-20 mt-1 overflow-auto leading-5 text-gray-500 max-w-screen-2xl text-md">
              {submission.content}
            </p>
          </div>
        </div>
        <div className="flex flex-col items-end justify-end shrink-0">
          <p className="text-sm leading-6 text-gray-900">
            <em>Posted: {dayjs(submission.created_at).fromNow()}</em>
          </p>
        </div>
      </div>
    </>
  );
}
